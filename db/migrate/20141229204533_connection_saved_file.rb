class ConnectionSavedFile < ActiveRecord::Migration
  def change
    create_table :connection_saved_files do |t|
      t.integer :connection_id
      t.integer :saved_file_id
      t.timestamps
    end
  end
end
