class GroupSavedFiles < ActiveRecord::Migration
  def change
      create_table :group_saved_files do |t|
      t.integer :connection_id
      t.integer :saved_file_id
    end
  end
end
