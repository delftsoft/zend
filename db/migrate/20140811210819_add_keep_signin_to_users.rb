class AddKeepSigninToUsers < ActiveRecord::Migration
  def change
    add_column :users, :keep_signin, :boolean
  end
end
