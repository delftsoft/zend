class UpdateContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :user_id, :integer
    add_column :contacts, :contact_id, :integer
  end
end
