class Groups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string     :name
      t.string     :description
      t.integer    :owner_id
      t.integer    :saved_file_id
      t.timestamps
    end
  end
end
