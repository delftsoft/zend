class GroupConnections < ActiveRecord::Migration
  def change
    create_table :group_connections do |t|
      t.integer     :group_id
      t.integer     :contact_id
      t.timestamps
    end
  end
end
