class SavedFiles < ActiveRecord::Migration
  def change
    create_table :saved_files do |t|
      t.string :name
      t.timestamps
    end
  end
end
