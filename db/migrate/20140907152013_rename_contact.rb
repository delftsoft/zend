class RenameContact < ActiveRecord::Migration
  def change
    rename_table :contacts, :connections
  end
end
