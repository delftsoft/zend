class Group < ActiveRecord::Base
belongs_to :owner, :class_name => "User"
has_many :group_connections
end
