class SavedFile < ActiveRecord::Base
has_many :connection_saved_files
has_many :connections, through: :connection_saved_files

# Mount the uploader to be able to upload files
mount_uploader :name, SavedFileUploader

end
