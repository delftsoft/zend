class User < ActiveRecord::Base
  has_many :authentications
  
  #Connections
  has_many :connections, :class_name => "Connection", :foreign_key => "user_id"
  has_many :contacts, through: :connections, :source => :contact
  
  #Connected by
  has_many :inverse_connections, :class_name => "Connection", :foreign_key => "contact_id"
  has_many :inverse_contacts, through: :inverse_connections, :source => :user
  
  #Group_connections
  has_many :group_connections, :foreign_key => "contact_id"
  has_many :groups, through: :group_connections
  
  #Owned groups
  has_many :owned_groups, :class_name => "Group", :foreign_key => "owner_id"
  
  #Method to find the given number of newest received files
  def find_newest_files(number_of_files)
    
    # Get the different connection_saved_files arrays from all the inverse connections and put them together in one array
    csfs = self.inverse_connections.map(&:connection_saved_files ).flatten
    
    # Sort the connectoin_saved_file array desc by the time the were updated
    csfs = csfs.sort_by{ |csf| csf.updated_at}.reverse
    
    # Return the first number of files from the list
    return csfs.take(number_of_files)
  end
  
  before_save {self.email = email.downcase}
  before_create :create_remember_token
  validates( :name,   presence: true, length: { maximum: 50 })
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates( :email,  presence: true, format: { with: VALID_EMAIL_REGEX}, uniqueness: {case_sensative: false})
  
  has_secure_password
  validates :password, length: { minimum: 6 }
  
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end
  
  #OmniAuth
  def from_omniauth(auth)
      self.name = auth.info.name
      self.email = auth.info.email
      tempPassword = SecureRandom.urlsafe_base64
      self.password = tempPassword
      self.password_confirmation = tempPassword
  end
  
  def password_required?
    (authentications.empty? || !password.blank?) && super
  end
  
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
  
  def email_taken?
    self.class.exists?(:email => email)
  end
  
  private

    def create_remember_token
      self.remember_token = User.digest(User.new_remember_token)
    end
end
