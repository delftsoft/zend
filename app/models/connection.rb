class Connection < ActiveRecord::Base
belongs_to :user
belongs_to :contact, :class_name => "User"
has_many :connection_saved_files
has_many :saved_files, through: :connection_saved_files
end
