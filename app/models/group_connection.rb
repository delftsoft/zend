class GroupConnection < ActiveRecord::Base
belongs_to :group
belongs_to :contact, :class_name => "User"
has_many :group_saved_files
has_many :saved_files, through: :group_saved_files
end
