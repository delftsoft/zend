module SessionsHelper

  #method to sign in user; Creating cookies and set current_user to loged in user
  def sign_in(user, keep_signin)
    remember_token = User.new_remember_token
    if keep_signin
      cookies.permanent[:remember_token] = remember_token
    else
      cookies[:remember_token] = remember_token
    end
    
    user.update_attribute(:remember_token, User.digest(remember_token))
    self.current_user = user
  end
  
  #returns boolean if current_user is set (user logged in or not)
  def signed_in?
    !current_user.nil?
  end
  
  def current_user=(user)
    @current_user = user
  end
  
  
  def current_user
    remember_token = User.digest(cookies[:remember_token])
    @current_user ||= User.find_by(remember_token: remember_token)
  end
  
  #sign out user (delete cookie and set current_user to nil)
  def sign_out
    current_user.update_attribute(:remember_token, User.digest(User.new_remember_token))
    cookies.delete(:remember_token)
    self.current_user = nil
  end
end