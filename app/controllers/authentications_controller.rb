class AuthenticationsController < ApplicationController
  include SessionsHelper #includes sessions_helper to use sessions_helper methods
  
  #if current user is present redirect to AuthenticationsViews:index (shows login for multiple social media login options (mainly develoment stuff))
  def index
    @authentications = current_user.authentications if current_user
  end


  #Called when callback from socialmedia login (route in routes.rb file)
  def create
      
      #request omniauth data and stores it in local variable    
      omniauth = request.env["omniauth.auth"]
      authentication = Authentication.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
      if authentication #login excisting user
        flash[:notice] = "Signed in successful."
        sign_in(authentication.user, true) #edit remember me input
        redirect_to authentications_url
      elsif current_user #add authentication to excising user
        current_user.authentications.create(:provider => omniauth['provider'], :uid => omniauth['uid'])
        flash[:notice] = "Authentication successful."
        redirect_to authentications_url
      else #create new user and sign in
        user = User.new
        user.from_omniauth(omniauth)
        user.authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
        
        #Check for email field. If not there response with error, need to be fixed to go to page for manual email submission
        if omniauth.info.email == nil
          flash[:error] = 'No email permissions obtained, please visit the FAQ page for more information'
        else
          if user.save
            flash[:notice] = "Signed in successful."
            sign_in(user, true) #edit remember me input
          elsif user.email_taken?
            flash[:error] = "Email already connected to excisting account"
          else
            flash[:error] = "Could not sign in."
          end
        end
        
        #rediret to authentication page !THIS NEEDS TO BE CHANGED!
        redirect_to authentications_url
        
      end

  end

  #destroys login from user (!development only!)
  def destroy
    @authentication = current_user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = "Successfully destroyed authentication."
    redirect_to authentications_url
  end
  
  #when callback fails from social media (eg devlined login)redirect to a page (root in this case) with an error message
  #!Need more implementation to show user friendly messages!
  def failure
    flash[:error] = params[:message].humanize
    redirect_to root_url
  end
end
