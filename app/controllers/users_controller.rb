class UsersController < ApplicationController
  
  #Redirect to user profile. First loading user and store it in @user variable to be used in UsersViews:show
  def show
    @user = User.find(params[:id])
  end
  
  #Redirect to a list of users: UserView:index
  def index
    @users = User.all
  end
  
  #Redirect to UsersViews:new where a user can create a new account
  def new
    @user = User.new
  end
  
  #Redirect to UserView:inbox where the file inbox of the user is showed
  def inbox
    @user = current_user
  end
  
  #gets form info from UsersView:new and handles it
  def create
    @user = User.new(user_params)    
    if @user.save
      sign_in @user, false
      flash[:success] = "Welcome to Zend!"
      redirect_to inbox_path
    else
      render 'new'
    end
  end
  
  private

    #method to set required parameters for creating account
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    
end
