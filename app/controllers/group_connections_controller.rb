 class GroupConnectionsController < ApplicationController
  
  def new
  end
  
  def create
    # Create a new group_connection based on the group id passed on
      @group_connection = GroupConnection.new(:group_id => params[:group_id], :contact_id => current_user.id)
    if @group_connection.save
      flash[:notice] = "Added group connection."
      redirect_to inbox_url
    else
      flash[:error] = "Unable to add group connection."
      redirect_to root_url
    end
  end
  
    def destroy
    @group_connection = current_user.connections.find(params[:id])
    @group_connection.destroy
    flash[:notice] = "Removed group contact."
    redirect_to inbox_url
  end
end