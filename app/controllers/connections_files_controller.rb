 class ConnectionsFilesController < ApplicationController
  
  def new
    @files = Files.all
  end
  
  def create
    @connection_file = current_connection.connections_files.build(:file_id => params[:file_id])
    if @connection_file.save
      flash[:notice] = "Added file."
      redirect_to inbox_url
    else
      flash[:error] = "Unable to add file."
      redirect_to root_url
    end
  end
  
    def destroy
    @connection_file = current_connection.connections_files.find(params[:id])
    @connection_file.destroy
    flash[:notice] = "Removed connection_file."
    redirect_to inbox_url
  end
  
end