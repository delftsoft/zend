class GroupsController < ApplicationController
  def new
    @group = Group.new
    @user = current_user
  end

  def create
    # Create a new group with the currenct user as owner
    @group = Group.new(:name => params[:group][:name], :description => params[:group][:description], :owner_id => current_user.id)
    
    # Add all the selected contacts as members of the group
    params[:contact_ids].each do |contact_id| 
      @group.group_connections.build(:contact_id => contact_id)
    end
    
    # Add yourself to the group
    @group.group_connections.build(:contact_id => current_user.id)
    if @group.save
      flash[:notice] = "Added group and members."
      redirect_to inbox_url
    else
      flash[:error] = "Unable to add group or members."
      redirect_to root_url
    end
  end

  def destroy
    @group = current_user.groups.find(params[:id])
    @group.destroy
    flash[:notice] = "Removed group."
    redirect_to inbox_url
  end
end

