 class ConnectionsController < ApplicationController
  
  def new
    @users = User.all
  end
  
  def create
    @connection = current_user.connections.build(:contact_id => params[:contact_id])
    if @connection.save
      flash[:notice] = "Added connection."
      redirect_to inbox_url
    else
      flash[:error] = "Unable to add connection."
      redirect_to root_url
    end
  end
  
    def destroy
    @connection = current_user.connections.find(params[:id])
    @connection.destroy
    flash[:notice] = "Removed contact."
    redirect_to inbox_url
  end
  
end