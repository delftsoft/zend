class SavedFilesController < ApplicationController
  #Redirect to file profile. First loading file and store it in @file variable to be used in filesViews:show
  def show
    @saved_file = SavedFile.find(params[:id])
  end

  #Redirect to a list of files: fileView:index
  def index
    @saved_files = SavedFile.all
  end

  #Redirect to filesViews:new where a file can create a new account
  def new
    @saved_file = SavedFile.new
  end

  #gets form info from filesView:new and handles it
  def create
    # Create a new saved file (Later we need to check if the file already exists)
    @saved_file = SavedFile.new(file_params)

    if @saved_file.save
      # Create a new connection_saved_file based on the connection id passed on
      @connection_saved_file = ConnectionSavedFile.new(:connection_id => params[:connection_id], :saved_file_id => @saved_file.id)
      if @connection_saved_file.save
        flash[:success] = "Your file is saved!"
        redirect_to inbox_url
      else
        render 'new'
      end
    end
  end

  private

  #method to set required parameters for creating saved_file
  def file_params
    params.require(:saved_file).permit(:name)
  end

end
