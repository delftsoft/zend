class SessionsController < ApplicationController
  #from /signin to SessionsViews:new
  def new
  end

  #gets login data from login form on SessionsViews:new
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in(user, params[:session][:keep_signin]) #signin method from sessions_helper
      redirect_to inbox_url
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  #Signout user
  def destroy
    sign_out #sign_out method from sessions_helper
    redirect_to root_url
  end
end
