OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, '822362369425-g5b524kgjqdv0pv5dkiu54hor9naqpm4.apps.googleusercontent.com', '1RCaLfnrGmhH8w_FC1B9kQ4h'
  
  provider :facebook, '892875014061362', '24eab336400f9406dec5de3bf00e514a'
end

OmniAuth.config.on_failure = Proc.new { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}
